mod helpers;
mod scraps;
mod statistics;
mod template_data;

pub mod writer;

type Iteration = Vec<f64>;
