use handlebars::{Context, Handlebars, Helper, HelperResult, Output, RenderContext};

/// Handlebar helper to format values as vec of vecs
pub fn format_y_values(
  h: &Helper,
  _: &Handlebars,
  _: &Context,
  _: &mut RenderContext,
  out: &mut dyn Output,
) -> HelperResult {
  let scale = h
    .param(0)
    .ok_or_else(|| handlebars::RenderError::new(&"Missing scale in template function call"))?
    .value()
    .as_f64()
    .ok_or_else(|| handlebars::RenderError::new(&"Scale value could not be converted into f64"))?;

  let values = h
    .param(1)
    .ok_or_else(|| handlebars::RenderError::new(&"Missing values in template function call"))?
    .value()
    .as_array()
    .ok_or_else(|| handlebars::RenderError::new(&"Values could not be converted into an array"))?;

  let formatted_values: Vec<Vec<usize>> = values
    .iter()
    .enumerate()
    .map(|(i, s)| return vec![(i as f64 * scale) as usize, (*s).as_f64().unwrap() as usize])
    .collect();

  out.write(&format!("{:?}", formatted_values))?;
  Ok(())
}

pub fn incremented(
  h: &Helper,
  _: &Handlebars,
  _: &Context,
  _: &mut RenderContext,
  out: &mut dyn Output,
) -> HelperResult {
  let index = h
    .param(0)
    .ok_or_else(|| handlebars::RenderError::new(&"Missing index in template function call"))?
    .value()
    .as_i64()
    .ok_or_else(|| handlebars::RenderError::new(&"Index value could not be converted into i64"))?;

  out.write(&format!("{:?}", index + 1))?;
  Ok(())
}

/// Handlebars helper to format values to display a dispersion area
pub fn format_dispersion_area(
  h: &Helper,
  _: &Handlebars,
  _: &Context,
  _: &mut RenderContext,
  out: &mut dyn Output,
) -> HelperResult {
  let scale = h
    .param(0)
    .ok_or_else(|| handlebars::RenderError::new(&"Missing scale in template function call"))?
    .value()
    .as_f64()
    .ok_or_else(|| handlebars::RenderError::new(&"Scale value could not be converted into f64"))?;

  let values_q1 = h
    .param(1)
    .ok_or_else(|| {
      handlebars::RenderError::new(&"Missing lower quartile values in template function call")
    })?
    .value()
    .as_array()
    .ok_or_else(|| {
      handlebars::RenderError::new(&"Lower quartile values could not be converted into an array")
    })?;

  let values_q3 = h
    .param(2)
    .ok_or_else(|| {
      handlebars::RenderError::new(&"Missing upper quartile values in template function call")
    })?
    .value()
    .as_array()
    .ok_or_else(|| {
      handlebars::RenderError::new(&"Upper quartile values could not be converted into an array")
    })?;

  let formatted_values: Vec<Vec<usize>> = values_q1
    .iter()
    .zip(values_q3.iter())
    .enumerate()
    .map(|(i, (q1, q3))| {
      return vec![
        (i as f64 * scale) as usize,
        (*q1).as_f64().unwrap() as usize,
        (*q3).as_f64().unwrap() as usize,
      ];
    })
    .collect();

  out.write(&format!("{:?}", formatted_values))?;
  Ok(())
}

/// Helper to compute CPU usage in percents
pub fn calculate_cpu_percent(
  previous_cpu: f64,
  previous_system: f64,
  cpu: f64,
  system: f64,
  cores: u64,
) -> f64 {
  let cpu_delta = (cpu - previous_cpu) as f64;
  let system_delta = (system - previous_system) as f64;

  (system_delta > 0.0 && cpu_delta > 0.0)
    .then(|| (cpu_delta / system_delta) * 100.0 * cores as f64)
    .unwrap_or_default()
}
