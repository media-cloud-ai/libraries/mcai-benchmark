use crate::{error::Result, report::helpers::calculate_cpu_percent, stats::Stats};
use enterpolation::{linear::Linear, Curve};

/// Data structure to store information streamed by the docker stats API
#[derive(Debug, Default)]
pub struct Scraps {
  timestamps: Vec<f64>,
  memory_usages: Vec<f64>,
  container_cpu_usages: Vec<f64>,
  system_cpu_usages: Vec<f64>,
  cpu_percents: Vec<f64>,
}

impl Scraps {
  pub fn get_cpu_percents(&self) -> &Vec<f64> {
    &self.cpu_percents
  }

  pub fn get_memory_usages(&self) -> &Vec<f64> {
    &self.memory_usages
  }

  pub fn add(&mut self, ts: i64, stats: &Stats) -> Result<()> {
    let cpu_percent = calculate_cpu_percent(
      *self.container_cpu_usages.last().unwrap_or(&0f64),
      *self.system_cpu_usages.last().unwrap_or(&0f64),
      stats.cpu.total_usage as f64,
      stats.cpu.system_cpu_usage.unwrap_or_default() as f64,
      stats.cpu_count,
    );

    self.timestamps.push(ts as f64);
    self.memory_usages.push(stats.memory.usage as f64);
    self.container_cpu_usages.push(stats.cpu.total_usage as f64);

    self
      .system_cpu_usages
      .push(stats.cpu.system_cpu_usage.unwrap_or_default() as f64);

    self.cpu_percents.push(cpu_percent);
    Ok(())
  }

  fn internal_interpolate_data(
    samples: usize,
    timestamps: &[f64],
    data: &mut Vec<f64>,
  ) -> Result<()> {
    *data = Linear::builder()
      .elements(data as &Vec<f64>)
      .knots(timestamps.to_vec())
      .build()?
      .take(samples)
      .collect();

    Ok(())
  }

  pub fn interpolate_data(&mut self, samples: usize) -> Result<()> {
    Self::internal_interpolate_data(samples, &self.timestamps, &mut self.memory_usages)?;
    Self::internal_interpolate_data(samples, &self.timestamps, &mut self.cpu_percents)?;
    Ok(())
  }
}
