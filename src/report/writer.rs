use crate::{
  benchmark::{measurement::Measurement, result::Results},
  error::{Error, Result},
  report::{
    helpers::{format_dispersion_area, format_y_values, incremented},
    scraps::Scraps,
    statistics::StatsSummary,
    template_data::{GraphTemplateData, IndexTemplateData, SummaryTemplateData},
    Iteration,
  },
  stats::{ConsoleWriter, Stats, StatsWriter},
};
use chrono::NaiveDateTime;
use handlebars::Handlebars;
use std::{
  collections::HashMap,
  sync::{Arc, Mutex},
};

#[derive(Default)]
/// Statistics writer that stores metrics and draws graphs when the benchmark ends
pub struct GraphWriter {
  stats: HashMap<String, Scraps>,
  streamed_writer: Option<Arc<Mutex<ConsoleWriter>>>,
}

impl GraphWriter {
  pub fn new() -> Self {
    GraphWriter {
      stats: HashMap::new(),
      ..Default::default()
    }
  }

  /// Configures a second writer to write streamed metrics to the console
  pub fn with_console_output(mut self) -> Self {
    self.streamed_writer = Some(Arc::new(Mutex::new(ConsoleWriter {})));
    self
  }

  /// Configures a second writer in which metrics will be streamed
  pub fn with_streamed_writer(mut self, streamed_writer: Arc<Mutex<ConsoleWriter>>) -> Self {
    self.streamed_writer = Some(streamed_writer);
    self
  }

  /// Interpolate data on a pre-calculated range
  fn interpolate_data(&mut self, measurement: &Measurement, samples: usize) -> Result<()> {
    measurement.get_containers_id().iter().try_for_each(|id| {
      self
        .stats
        .get_mut(id)
        .ok_or_else(|| Error::MissingId(format!("Missing id {}", id)))?
        .interpolate_data(samples)
    })?;

    Ok(())
  }

  fn get_cpu_values(&self, measurement: &Measurement) -> Vec<Iteration> {
    measurement
      .get_containers_id()
      .iter()
      .map(|id| self.stats.get(id).unwrap().get_cpu_percents().clone())
      .collect::<Vec<Iteration>>()
  }

  fn get_memory_values(&self, measurement: &Measurement) -> Vec<Iteration> {
    measurement
      .get_containers_id()
      .iter()
      .map(|id| self.stats.get(id).unwrap().get_memory_usages().clone())
      .collect::<Vec<Iteration>>()
  }
}

impl StatsWriter for GraphWriter {
  fn next(&mut self, stats: &Stats) -> Result<()> {
    if stats.timestamp == "0001-01-01 00:00:00 UTC" {
      return Ok(());
    }

    let ts = NaiveDateTime::parse_from_str(&stats.timestamp, "%Y-%m-%d %H:%M:%S.%f UTC")
      .map_err(Error::Timestamp)?
      .timestamp_nanos();

    if let Some(scraps) = self.stats.get_mut(&stats.container_id) {
      scraps.add(ts, stats)?;
    } else {
      let mut scraps = Scraps::default();
      scraps.add(ts, stats)?;
      self.stats.insert(stats.container_id.clone(), scraps);
    }

    // If a second writer is configured, stream metrics
    if let Some(streamed_writer) = &self.streamed_writer {
      streamed_writer.lock().unwrap().next(stats)?;
    }

    Ok(())
  }

  fn end(&mut self, results: &Results) -> Result<()> {
    let mut handlebars = Handlebars::new();
    handlebars.register_helper("format_y_values", Box::new(format_y_values));
    handlebars.register_helper("format_dispersion_area", Box::new(format_dispersion_area));
    handlebars.register_helper("incremented", Box::new(incremented));

    let index_template = include_str!("./templates/index.html");
    let summary_template = include_str!("./templates/comparison_graphs.html");
    let graph_template = include_str!("./templates/detailed_graphs.html");

    let stages_names: Vec<String> = results.get_stages_names();

    for (name, result) in results.get_benchmark_result() {
      let mut inner_html: Vec<String> = Vec::with_capacity(&result.get_measurements().len() + 1); // This is to store templated HTML graphs

      let summary_template_data: SummaryTemplateData = result.into();
      inner_html.push(handlebars.render_template(summary_template, &summary_template_data)?);

      for measurement in result.get_measurements() {
        let scale = 1.0;
        let samples = (measurement.get_median_duration() / scale) as usize; // Take 1 data points per second

        self.interpolate_data(measurement, samples)?;

        let data = GraphTemplateData::new(name.to_string(), scale, &measurement.hardware)
          .with_cpu_distribution(StatsSummary::from(self.get_cpu_values(measurement)))
          .with_memory_distribution(StatsSummary::from(self.get_memory_values(measurement)))
          .with_durations(measurement.get_durations());

        inner_html.push(handlebars.render_template(graph_template, &data)?);
      }

      let report_output = handlebars.render_template(
        index_template,
        &IndexTemplateData::new(inner_html, stages_names.clone()),
      )?;

      std::fs::write(
        format!(
          "./{}/{}.html",
          results
            .get_config()
            .get_output_folder()
            .unwrap_or_else(|| "benchmark".to_string()),
          name
        ),
        report_output,
      )?;
    }

    if let Some(streamed_writer) = &self.streamed_writer {
      streamed_writer.lock().unwrap().end(results)?;
    }

    Ok(())
  }

  fn error(&mut self, error: &Error) {
    println!("Error: {:?}", error);
  }
}
