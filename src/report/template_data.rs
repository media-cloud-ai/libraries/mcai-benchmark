use crate::{
  benchmark::{measurement::Hardware, result::BenchmarkResult},
  report::{statistics::StatsSummary, Iteration},
};
use serde::Serialize;

/// Data structure used to build index template with handlebars
#[derive(Clone, Serialize)]
pub struct IndexTemplateData {
  inner_html: Vec<String>,
  stages: Vec<String>,
}

impl IndexTemplateData {
  pub fn new(inner_html: Vec<String>, stages: Vec<String>) -> Self {
    IndexTemplateData { inner_html, stages }
  }
}

/// Data structure used to build summary template with handlebars
#[derive(Clone, Default, Serialize)]
pub struct SummaryTemplateData {
  hardware_details: Vec<(f32, i64)>,
  durations: Vec<Iteration>,
}

impl From<&BenchmarkResult> for SummaryTemplateData {
  fn from(result: &BenchmarkResult) -> Self {
    let hardware_details = result
      .get_measurements()
      .iter()
      .map(|m| (m.hardware.cpu, m.hardware.memory))
      .collect();

    let durations = result
      .get_measurements()
      .iter()
      .map(|m| {
        let mut d: Vec<f64> = m.iterations.iter().map(|i| i.duration).collect();
        d.sort_by(|a, b| a.partial_cmp(b).unwrap());
        d
      })
      .collect();

    SummaryTemplateData {
      hardware_details,
      durations,
    }
  }
}

/// Data structure used to build graph template with handlebars
#[derive(Clone, Default, Serialize)]
pub struct GraphTemplateData {
  id: String,
  name: String,
  scale: f64,
  durations: Vec<f64>,
  cpu_amount: f32,
  cpu_distribution: StatsSummary,
  memory_amount: i64,
  memory_distribution: StatsSummary,
}

impl GraphTemplateData {
  pub fn new(name: String, scale: f64, hardware: &Hardware) -> Self {
    let id = format!("{}-{}-{}", name, hardware.cpu, hardware.memory);

    GraphTemplateData {
      id,
      name,
      scale,
      cpu_amount: hardware.cpu,
      memory_amount: hardware.memory,
      ..Default::default()
    }
  }

  pub fn with_durations(mut self, durations: Vec<f64>) -> Self {
    self.durations = durations;
    self
  }

  pub fn with_cpu_distribution(mut self, cpu_distribution: StatsSummary) -> Self {
    self.cpu_distribution = cpu_distribution;
    self
  }

  pub fn with_memory_distribution(mut self, memory_distribution: StatsSummary) -> Self {
    self.memory_distribution = memory_distribution;
    self
  }
}
