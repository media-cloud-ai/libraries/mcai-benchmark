use crate::report::Iteration;
use serde::Serialize;

#[derive(Clone, Default, Serialize)]
struct StatsSerie {
  values: Vec<f64>,
}

impl StatsSerie {
  fn new(capacity: usize) -> Self {
    StatsSerie {
      values: Vec::with_capacity(capacity),
    }
  }

  pub fn push(&mut self, value: f64) {
    self.values.push(value);
  }
}

/// Structure containing statistics showed on report graphs.
#[derive(Clone, Default, Serialize)]
pub struct StatsSummary {
  q1: StatsSerie,
  q2: StatsSerie,
  q3: StatsSerie,
}

impl StatsSummary {
  fn new(capacity: usize) -> Self {
    StatsSummary {
      q1: StatsSerie::new(capacity),
      q2: StatsSerie::new(capacity),
      q3: StatsSerie::new(capacity),
    }
  }
}

impl From<Vec<Iteration>> for StatsSummary {
  fn from(series: Vec<Iteration>) -> Self {
    let number_of_iterations = series.len();
    let number_of_samples = series[0].len();

    let mut summary = StatsSummary::new(number_of_samples);

    for j in 0..number_of_samples {
      let mut values: Vec<f64> = series.iter().map(|s| s[j]).collect();
      values.sort_by(|a, b| a.partial_cmp(b).unwrap());

      summary.q1.push(values[number_of_iterations / 4_usize]);
      summary.q2.push(values[number_of_iterations / 2_usize]);
      summary.q3.push(values[number_of_iterations / 4 * 3_usize]);
    }
    summary
  }
}
