use crate::{
  benchmark::result::Results,
  error::{Error, Result},
};
use bollard::container;
use serde::Serialize;
use serde_json;
use std::collections::HashMap;

pub trait StatsWriter {
  fn next(&mut self, stats: &Stats) -> Result<()>;

  fn end(&mut self, results: &Results) -> Result<()>;

  fn error(&mut self, error: &Error);
}

/// Basic statistics writer to output statistics to the console.
pub struct ConsoleWriter {}

impl StatsWriter for ConsoleWriter {
  fn next(&mut self, stats: &Stats) -> Result<()> {
    println!("{}", serde_json::to_string(stats).unwrap());
    Ok(())
  }

  fn end(&mut self, _results: &Results) -> Result<()> {
    println!("End of statistics stream.");
    Ok(())
  }

  fn error(&mut self, error: &Error) {
    println!("Error: {:?}", error);
  }
}

/// Statistics collected on running containers.
#[derive(Clone, Debug, Serialize)]
pub struct Stats {
  pub timestamp: String,
  pub container_id: String,
  pub cpu: CpuStats,
  pub memory: MemoryStats,
  pub networks: Option<HashMap<String, NetworkInterfaceStats>>,
  pub storage: StorageStats,
  pub cpu_count: u64,
}

impl From<container::Stats> for Stats {
  fn from(stats: container::Stats) -> Self {
    let networks_stats: Option<HashMap<String, NetworkInterfaceStats>> =
      stats.networks.map(|nets| {
        HashMap::from_iter(
          nets
            .iter()
            .map(|(name, st)| (name.to_string(), (*st).into())),
        )
      });

    Stats {
      timestamp: stats.read.to_string(),
      container_id: stats.id,
      cpu_count: stats.cpu_stats.online_cpus.unwrap_or(1),
      cpu: stats.cpu_stats.into(),
      memory: stats.memory_stats.into(),
      networks: networks_stats,
      storage: stats.storage_stats.into(),
    }
  }
}

/// CPU statistics on the running container.
#[derive(Clone, Debug, Serialize)]
pub struct CpuStats {
  pub per_cpu_usage: Option<Vec<u64>>,
  pub usage_in_user_mode: u64,
  pub total_usage: u64,
  pub usage_in_kernel_mode: u64,
  pub system_cpu_usage: Option<u64>,
}

impl From<container::CPUStats> for CpuStats {
  fn from(cpu: container::CPUStats) -> Self {
    CpuStats {
      per_cpu_usage: cpu.cpu_usage.percpu_usage,
      usage_in_user_mode: cpu.cpu_usage.usage_in_usermode,
      total_usage: cpu.cpu_usage.total_usage,
      usage_in_kernel_mode: cpu.cpu_usage.usage_in_kernelmode,
      system_cpu_usage: cpu.system_cpu_usage,
    }
  }
}

/// Memory statistics on the running container.
#[derive(Clone, Debug, Serialize)]
pub struct MemoryStats {
  pub usage: u64,
  pub max_usage: u64,
}

impl From<container::MemoryStats> for MemoryStats {
  fn from(memory: container::MemoryStats) -> Self {
    MemoryStats {
      usage: memory.usage.unwrap_or_default(),
      max_usage: memory.max_usage.unwrap_or_default(),
    }
  }
}

/// Network IO statistics on the running container.
#[derive(Clone, Debug, Serialize)]
pub struct NetworkInterfaceStats {
  pub rx_bytes: u64,
  pub rx_dropped: u64,
  pub rx_errors: u64,
  pub rx_packets: u64,
  pub tx_bytes: u64,
  pub tx_dropped: u64,
  pub tx_errors: u64,
  pub tx_packets: u64,
}

impl From<container::NetworkStats> for NetworkInterfaceStats {
  fn from(network: container::NetworkStats) -> Self {
    NetworkInterfaceStats {
      rx_bytes: network.rx_bytes,
      rx_dropped: network.rx_dropped,
      rx_errors: network.rx_errors,
      rx_packets: network.rx_packets,
      tx_bytes: network.tx_bytes,
      tx_dropped: network.tx_dropped,
      tx_errors: network.tx_errors,
      tx_packets: network.tx_packets,
    }
  }
}

/// Storage IO statistics on the running container.
#[derive(Clone, Debug, Serialize)]
pub struct StorageStats {
  pub read_count_normalized: Option<u64>,
  pub read_size_bytes: Option<u64>,
  pub write_count_normalized: Option<u64>,
  pub write_size_bytes: Option<u64>,
}

impl From<container::StorageStats> for StorageStats {
  fn from(storage: container::StorageStats) -> Self {
    StorageStats {
      read_count_normalized: storage.read_count_normalized,
      read_size_bytes: storage.read_size_bytes,
      write_count_normalized: storage.write_count_normalized,
      write_size_bytes: storage.write_size_bytes,
    }
  }
}
