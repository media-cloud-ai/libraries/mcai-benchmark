use crate::{
  benchmark::{Run, RunConfig},
  error::Result,
  stats::Stats,
};
use async_trait::async_trait;
use bollard::container::StatsOptions;
use byte_unit::{Byte, ByteUnit};
use chrono::Utc;
use futures_util::stream::StreamExt;
use mcai_docker::image;
use serde::Serialize;
use std::{
  fs::File,
  io::{stdout, Write},
  time::Instant,
};

#[derive(Clone, Debug, Serialize)]
pub struct Iteration {
  pub container_id: String,
  pub duration: f64,
}

impl Iteration {
  pub fn new(container_id: &str, duration: f64) -> Self {
    Iteration {
      container_id: container_id.to_string(),
      duration,
    }
  }
}

#[derive(Debug, Serialize)]
pub struct Hardware {
  pub cpu: f32,
  pub memory: i64,
}

#[derive(Debug, Serialize)]
pub struct Measurement {
  pub hardware: Hardware,
  pub iterations: Vec<Iteration>,
}

impl Measurement {
  fn open_writer(
    image: &str,
    output_folder: &Option<String>,
    benchmark: Option<&str>,
  ) -> Result<Box<dyn Write + Send>> {
    if let Some(output_folder) = output_folder {
      let benchmark = benchmark
        .map(|name| format!("_{}", name))
        .unwrap_or_default();

      let image = image.split('/').last().unwrap_or_default();

      let filepath = format!(
        "{}/{}{}_{}.json",
        output_folder,
        image,
        benchmark,
        Utc::now().format("%Y%m%d_%H%M%S")
      );
      Ok(Box::new(
        File::options().create(true).append(true).open(filepath)?,
      ))
    } else {
      Ok(Box::new(stdout()))
    }
  }

  /// Allows retrieving all durations for a given measurement
  pub fn get_durations(&self) -> Vec<f64> {
    self.iterations.iter().map(|i| i.duration).collect()
  }

  pub fn get_median_duration(&self) -> f64 {
    let mut sorted_durations = self.get_durations();
    sorted_durations.sort_by(|a, b| a.partial_cmp(b).unwrap());
    sorted_durations[sorted_durations.len() / 2_usize]
  }

  /// Allows retrieving all containers id for a given measurement
  pub fn get_containers_id(&self) -> Vec<String> {
    self
      .iterations
      .iter()
      .map(|i| i.container_id.clone())
      .collect()
  }
}

#[async_trait]
impl Run for Measurement {
  async fn run(&mut self, config: &mut RunConfig) -> Result<()> {
    let docker = mcai_docker::Docker::connect_with_socket_defaults().unwrap();

    let envs: Vec<(&str, &(dyn ToString + Send + Sync))> = config
      .envs
      .iter()
      .map(|(key, value)| {
        let value: &(dyn ToString + Send + Sync) = value;
        (key.as_str(), value)
      })
      .collect();

    let vols: Vec<(&str, String)> = config
      .volumes
      .iter()
      .map(|vol| {
        let container_volume = if vol.readonly {
          format!("{},readonly", vol.container)
        } else {
          vol.container.to_string()
        };
        (vol.host.as_str(), container_volume)
      })
      .collect();

    let memory = Byte::from_unit(self.hardware.memory as f64, ByteUnit::MiB).unwrap();

    for _ in 0..self.iterations.capacity() {
      let builder = image::Image::new(&config.image)
        .build(&docker)
        .await?
        .with_env(
          "SOURCE_ORDERS",
          &format!("/examples/{}.json", config.source_order),
        )
        .with_env("ONLY_JSON_LOGS", &1)
        .with_env("RUST_LOG", &"DEBUG")
        .with_envs(envs.as_slice())
        .with_cpu_limit(self.hardware.cpu)
        .with_ram_limit(memory.get_bytes() as i64)
        .with_volume_bindings(
          vols
            .iter()
            .map(|(host, container)| (*host, container.as_str()))
            .collect::<Vec<(&str, &str)>>()
            .as_slice(),
        )
        .with_enable_tty();

      let writer = Self::open_writer(&config.image, &config.output_folder, Some(&config.name))?;
      let instance = builder.build(&docker).await?;

      let instance_id = instance.id.clone();
      let cloned_docker = docker.clone();

      if let Some(stats_writer) = config.stats_writer.clone() {
        tokio::spawn(async move {
          let stream = &mut cloned_docker.stats(
            &instance_id,
            Some(StatsOptions {
              stream: true,
              ..Default::default()
            }),
          );

          while let Some(Ok(stats)) = stream.next().await {
            let statistics: Stats = stats.clone().into();
            if let Err(error) = stats_writer.lock().unwrap().next(&statistics) {
              stats_writer.lock().unwrap().error(&error)
            };
          }
        });
      }

      let now = Instant::now();
      instance.run(&docker, Some(Box::leak(writer))).await?;

      self
        .iterations
        .push(Iteration::new(&instance.id, now.elapsed().as_secs_f64()));

      instance.remove(&docker, true).await?;
    }
    Ok(())
  }
}
