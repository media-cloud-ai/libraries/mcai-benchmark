use crate::{configuration::VolumeConfig, error::Result, stats::StatsWriter};
use async_trait::async_trait;
use std::{
  collections::HashMap,
  sync::{Arc, Mutex},
};

pub mod measurement;
pub mod result;

#[derive(Default)]
pub(crate) struct RunConfig {
  pub name: String,
  pub image: String,
  pub output_folder: Option<String>,
  pub source_order: String,
  pub stats_writer: Option<Arc<Mutex<dyn StatsWriter + Send + Sync>>>,
  pub envs: HashMap<String, String>,
  pub volumes: Vec<VolumeConfig>,
}

#[async_trait]
trait Run {
  async fn run(&mut self, config: &mut RunConfig) -> Result<()>;
}
