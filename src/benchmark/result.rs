use crate::{
  benchmark::{
    measurement::{Hardware, Measurement},
    Run, RunConfig,
  },
  configuration::{BenchmarkConfig, Configuration},
  error::Result,
  stats::StatsWriter,
};
use async_trait::async_trait;
use serde::Serialize;
use std::{
  collections::HashMap,
  sync::{Arc, Mutex},
};

#[derive(Debug, Serialize)]
pub struct Results {
  config: Configuration,
  benchmark_result: HashMap<String, BenchmarkResult>,
}

impl Results {
  pub fn get_config(&self) -> &Configuration {
    &self.config
  }

  pub fn get_benchmark_result(&self) -> &HashMap<String, BenchmarkResult> {
    &self.benchmark_result
  }
}

impl From<Configuration> for Results {
  fn from(config: Configuration) -> Self {
    let Configuration::Version1(config_v1) = &config;
    let benchmarks = config_v1
      .benchmarks
      .iter()
      .map(|(key, benchmark_config)| (key.clone(), benchmark_config.into()));

    let benchmark_result = HashMap::from_iter(benchmarks);

    Results {
      config,
      benchmark_result,
    }
  }
}

#[async_trait]
impl Run for Results {
  async fn run(&mut self, config: &mut RunConfig) -> Result<()> {
    for (name, benchmark) in &mut self.benchmark_result {
      config.envs = self.config.get_envs_for_job(name.as_str());
      config.volumes = self.config.get_volumes_for_job(name.as_str());
      config.name = name.to_string();
      (*benchmark).run(config).await?;
    }
    Ok(())
  }
}

impl Results {
  /// Run all benchmarks defined in the configuration file sequentially.
  pub async fn run_benchmark(
    &mut self,
    stats_writer: Option<Arc<Mutex<dyn StatsWriter + Send + Sync>>>,
  ) -> Result<()> {
    let cloned_stats_writer = stats_writer.clone();
    let mut run_config = RunConfig {
      image: self.config.get_worker_docker_image(),
      output_folder: self.config.get_output_folder(),
      stats_writer: cloned_stats_writer,
      ..Default::default()
    };

    self.run(&mut run_config).await?;

    if let Some(stats) = stats_writer {
      stats.lock().unwrap().end(self)?;
    }
    Ok(())
  }

  /// Getter for stages name
  pub fn get_stages_names(&self) -> Vec<String> {
    self
      .benchmark_result
      .iter()
      .map(|(name, _)| name.to_string())
      .collect()
  }
}

#[derive(Debug, Serialize)]
pub struct BenchmarkResult {
  source_order: String,
  measurements: Vec<Measurement>,
}

impl BenchmarkResult {
  pub fn get_measurements(&self) -> &Vec<Measurement> {
    &self.measurements
  }
}

impl From<&BenchmarkConfig> for BenchmarkResult {
  fn from(config: &BenchmarkConfig) -> Self {
    let measurements = config
      .get_hardware_configurations()
      .unwrap()
      .iter()
      .map(|(memory, cpu)| Measurement {
        hardware: Hardware {
          memory: *memory,
          cpu: *cpu,
        },
        iterations: Vec::with_capacity(config.iterations as usize),
      })
      .collect();

    BenchmarkResult {
      source_order: config.source_order.to_string(),
      measurements,
    }
  }
}

#[async_trait]
impl Run for BenchmarkResult {
  async fn run(&mut self, config: &mut RunConfig) -> Result<()> {
    for measure in &mut self.measurements {
      config.source_order = self.source_order.to_string();
      measure.run(config).await?;
    }
    Ok(())
  }
}
