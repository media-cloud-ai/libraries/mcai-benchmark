//! # MCAI Benchmark
//!
//! This crate allows to benchmarl MCAI wroker based on a configuration file.
//!
//! ## Configuration
//!
//! This is a sample configuration file.
//!
//! ```yaml
//! version: 1
//! output_folder: "./output_folder"
//! worker_docker_image: "image_name"
//!
//! my_benchmark_name:
//!   source_order: "source_order_filename"
//!   iterations: 5
//!   hardware:
//!   cpu:
//!     - 1
//!     - 2
//!   memory:
//!     - 2048
//!   envs:
//!     EXAMPLE_VAR: "my_example_var"
//!   volumes:
//!     - host: /path/to/a/volume
//!       container: /volume
//!       readonly: true
//! ```
//!
//! ## Usage
//!
//! ```no_run
//! use mcai_benchmark::{
//!     benchmark::result::Results,
//!     configuration::{Configuration, Version1},
//!     stats::ConsoleWriter,
//! };
//! use std::sync::{Arc, Mutex};
//!
//! #[tokio::main]
//! async fn main() {
//!     let config = Version1::read_from_file("./examples/sample_config.yaml").unwrap();
//!     let mut benchmark = Results::from(Configuration::Version1(config));
//!
//!     if let Err(error) = benchmark
//!         .run_benchmark(Some(Arc::new(Mutex::new(ConsoleWriter {}))))
//!         .await
//!     {
//!         println!("Error: {:?}", error);
//!     }
//!
//!     println!("{:?}", benchmark);
//! }
//! ```

pub mod benchmark;
pub mod configuration;
pub mod error;
pub mod report;
pub mod stats;
