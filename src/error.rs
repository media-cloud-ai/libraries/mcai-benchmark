pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
  Docker(mcai_docker::error::Error),
  Interpolation(enterpolation::linear::LinearError),
  Handlebars(handlebars::RenderError),
  MissingId(String),
  ReadConfiguration(std::io::Error),
  SerdeYaml(serde_yaml::Error),
  Timestamp(chrono::ParseError),
}

impl From<std::io::Error> for Error {
  fn from(error: std::io::Error) -> Self {
    Error::ReadConfiguration(error)
  }
}

impl From<serde_yaml::Error> for Error {
  fn from(error: serde_yaml::Error) -> Self {
    Error::SerdeYaml(error)
  }
}

impl From<mcai_docker::error::Error> for Error {
  fn from(error: mcai_docker::error::Error) -> Self {
    Error::Docker(error)
  }
}

impl From<enterpolation::linear::LinearError> for Error {
  fn from(error: enterpolation::linear::LinearError) -> Self {
    Error::Interpolation(error)
  }
}

impl From<chrono::ParseError> for Error {
  fn from(error: chrono::ParseError) -> Self {
    Error::Timestamp(error)
  }
}

impl From<handlebars::RenderError> for Error {
  fn from(error: handlebars::RenderError) -> Self {
    Error::Handlebars(error)
  }
}
